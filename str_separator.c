/*!
 * file: str_separator.h
 * Separation string function
 */

#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <stdlib.h>

typedef enum {
    OK, ERROR
} error_t;

error_t argz_create_sep(const char *string, int sep, char **argz, size_t *argz_len) {
    unsigned int i, n;

    n = strlen(string);
    *argz = (char *) malloc(127 * sizeof(char));

    printf("\nstring:");
    for (i = 0; i < n; i++) {
        printf("%c", string[i]);
    }

    strcpy(*argz, string);

    *argz_len = strlen(*argz);


    for (i = 0; i < *argz_len; i++) {
        if ((*argz)[i] == sep) {
            (*argz)[i] = 0;
        }
    }

}

int main() {


    char *const string = "SHELL=/bin/bash:usr=monty:PWD=/bin/monty:LANG=en_US.UTF-8";
    char *argz;
    int argz_len, i;

    argz_create_sep(string, 58, &argz, (size_t *) &argz_len);

    printf("\n\nLenght=%d", argz_len);

    printf("\nargz:");
    for (i = 0; i < argz_len; i++) {
        printf("%c", argz[i]);
    }

    getch();
    return 0;
}

